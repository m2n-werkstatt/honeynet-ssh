# SSH Server for Honeypots (used in the Honeynet Project)

> IMPORTANT : Create `/var/private/honeynet/login-attempts/` first

> IMPORTANT : Look at what user has `sudo` capabilities by default (user `admin`?)

Login attempts (connection attempts) before a user is created can be set in sshd_conf (see below). Currently the number of connection attempts is counted, not the number of tried passwords (default: 3 password tries per connection/login attempt). You may set the number of password tries per connection attempt to 1 if you want a one to one correspondence.

A new user is only created if NO user with that username exists (so the root password or other users CANNOT be compromised)

## Configuration and building

The `configure.sh` script contains two environment variables used to configure the SSH server

`SSH_PATH` sets the directory the server is installed to
`SSH_CONFIG_PATH` sets the directory which contains the `sshd_conf` file

The `build.sh` script contains the needed commands used to build and install the server. (GNU automake, autoreconf & autoconf needed). This does NOT create or register any systemd services.

The `start.sh` and `debug.sh` scripts start the server in normal / debug mode. If the server is started by theses scripts, it is executed normally, NOT as a daemon or service.

The `viconf.sh` script can be used to modifiy the `sshd_conf` file of this server.

### sshd_conf options

The `HoneypotLoginAttempts` option can be used to set the number of login attempts after which a honeypot user is created (default: INT_MAX)

## Logging

The SSH server writes 4 honeypot specific log messages:

1. Creating honeypot user:
   > Honeypot user `USERNAME` created by `IP`
2. Setting password of honeypot user:
   > Honeypot user `USERNAME` password set to : `PASSWORD`
3. Failed login:
   > PassLog: Username: `USERNAME` Password: `PASSWORD` Remote-IP: `IP` Local-IP: `IP` Local-Port: `PORT`
4. If honeypot user password cannot be set:
   > Honeypot user `USERNAME` deleted