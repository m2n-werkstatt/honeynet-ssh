#!/usr/bin/bash

# Read configuration
source configure.sh

# Start SSHD (no daemon)
${SSH_PATH}sbin/sshd -D
