#!/usr/bin/bash

# Read configuration
source configure.sh

# Go to src/ directory
cd ../src

# Generate build and make files
autoreconf
autoconf

# Compile and install
./configure --with-pam --prefix=${SSH_PATH} --sysconfdir=${SSH_CONFIG_PATH}
make
make install
